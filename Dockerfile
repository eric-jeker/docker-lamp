FROM ubuntu:latest

# Install apache, PHP, and supplimentary programs. openssh-server, curl, and lynx-cur are for debugging the container.
RUN apt-get update && \
    apt-get -y upgrade && \
    DEBIAN_FRONTEND=noninteractive apt-get -y install \
    apache2 \
    php7.0 \
    php7.0-cli \
    php7.0-common \
    php7.0-json \
    php7.0-mysql \
    php7.0-mbstring \
    php7.0-intl  \
    php7.0-gd  \
    php7.0-curl  \
    php7.0-zip  \
    php7.0-xml \
    php-xdebug \
    libapache2-mod-php7.0 \
    curl \
    lynx-cur \
    mysql-client \
    vim

# Enable apache mods.
RUN a2enmod php7.0
RUN a2enmod rewrite

# Update the PHP.ini file, enable <? ?> tags and quieten logging.
RUN sed -i "s/short_open_tag = Off/short_open_tag = On/" /etc/php/7.0/apache2/php.ini
RUN sed -i "s/error_reporting = .*$/error_reporting = E_ERROR | E_WARNING | E_PARSE/" /etc/php/7.0/apache2/php.ini

# Enable xdebug remote debugging
RUN echo "xdebug.idekey = PHPSTORM" >> /etc/php/7.0/apache2/conf.d/20-xdebug.ini
RUN echo "xdebug.default_enable = 0" >> /etc/php/7.0/apache2/conf.d/20-xdebug.ini
RUN echo "xdebug.remote_enable = 1" >> /etc/php/7.0/apache2/conf.d/20-xdebug.ini
RUN echo "xdebug.remote_autostart = 0" >> /etc/php/7.0/apache2/conf.d/20-xdebug.ini
RUN echo "xdebug.remote_connect_back = 0" >> /etc/php/7.0/apache2/conf.d/20-xdebug.ini
RUN echo "xdebug.profiler_enable = 0" >> /etc/php/7.0/apache2/conf.d/20-xdebug.ini
RUN echo "xdebug.remote_host = 192.168.8.102" >> /etc/php/7.0/apache2/conf.d/20-xdebug.ini

# Manually set up the apache environment variables
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid

# Expose apache.
EXPOSE 80

# Update the default apache site with the config we created.
ADD dev/apache-config.conf /etc/apache2/sites-enabled/000-default.conf

# By default start up apache in the foreground, override with /bin/bash for interactive.
CMD /usr/sbin/apache2ctl -D FOREGROUND

