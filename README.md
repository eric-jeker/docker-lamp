# Docker LAMP

This is Docker configuration to set up a LAMP container.

## Installation

Configuration the VHOST in `dev/apache-config.conf`.

Then run:

```
docker-compose up
```